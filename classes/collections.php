<?php

//put your API Key and Secret in these two variables.
define('USER_ID', '604b2153-11e7-4d30-8329-061d7f3504c4'); // API user id
define('API_KEY', 'a01ea80380d34a1f94ada0ecb6731ee1'); // API kEY
define('COLLECTION_SUBSCRIPTION_KEY', '0b82b67c254948abb46adedce00583a4'); // Collection subscription key

//Test here
//get_accesstoken();
echo requestToPay();

//When called this function will request an Access Token
function get_accesstoken(){

    $credentials = base64_encode(USER_ID.':'.API_KEY);

    //https://ericssonbasicapi1.azure-api.net/collection/token/

    $ch = curl_init("https://sandbox.momodeveloper.mtn.com/collection/token/");
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

    curl_setopt(
        $ch,
        CURLOPT_HTTPHEADER,
        array(
            'Authorization: Basic '.$credentials,
            'Content-Type: application/json',
            'Ocp-Apim-Subscription-Key: '.COLLECTION_SUBSCRIPTION_KEY
        )
    );

    $response = curl_exec($ch);

    // echo "<pre>";
    // var_dump($response);

    $response = json_decode($response);

   $access_token = $response->access_token;
    if(!$access_token){
        throw new Exception("Invalid access token generated");
        return FALSE;
    }
    return $access_token;

  }

  // request payment from customer
  function requestToPay(){

    //$access_token = get_accesstoken();
    $access_token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJSMjU2In0.eyJjbGllbnRJZCI6IjYwNGIyMTUzLTExZTctNGQzMC04MzI5LTA2MWQ3ZjM1MDRjNCIsImV4cGlyZXMiOiIyMDIwLTA5LTAyVDEyOjAzOjUyLjI2MiIsInNlc3Npb25JZCI6IjNhNWRjYTc4LTc0NTEtNDY4Zi1iMGY1LThiNjliZDJjNTc1NyJ9.cZzffIJQJb9z1GGmiAeujfCu5-MNl5EHmI4jt6z0VXYmzd4P_ChTgeJjL-DNsmMuC6rXveVtKfTqyh9kdxjt0wgZv_T2x5TnRG7gX_E4Vv_m9kIOKBlSqOVxtA9iBn1laHnFQJ64umPSy02Ie7TFmoho6P4vq3h_yyLyB-zmvBOXqDXtwteOO2kZhGA_FM4NDfdhlb7bm3t_qufuVC7n1u1hX0SDzh50SXDpx2o1QPK71dxh6TnIlDuWasNGUJetrNWT1fTFSSF5UItDdlMXl3RWOU8ozXc7d8St3Qnw52zqyNBsJ6wYTwapynDJeP3n_CfGQWhxuf5E1rvksd6WDQ";
    $endpoint_url = 'https://sandbox.momodeveloper.mtn.com/collection/v1_0/requesttopay';

    //https://ericssonbasicapi1.azure-api.net/collection/v1_0/requesttopay

    # Parameters
    $data = array(
          "amount" => "1",
          "currency" => "EUR", //default for sandbox
          "externalId" => "123456", //reference number

          "payer" => array(

              "partyIdType" => "MSISDN",
              "partyId"     => "46733123454"  //user phone number, these are test numbers)
          ),

          "payerMessage"=> "Payment Request",
          "payeeNote"=> "Please confirm payment"


        );

    $data_string = json_encode($data);

    $curl = curl_init();
    curl_setopt($curl, CURLOPT_URL, $endpoint_url);
    curl_setopt($curl, CURLOPT_CUSTOMREQUEST, "POST");
    //curl_setopt($curl, CURLOPT_TIMEOUT, 50);
    curl_setopt($curl, CURLOPT_POSTFIELDS, $data_string);

    curl_setopt(
        $curl,
        CURLOPT_HTTPHEADER,
        array(
            'Content-Type: application/json',  //optional
            'Authorization: Bearer '.$access_token, //optional
            'X-Callback-Url: https://localhost/moodle', //optional, not required for sandbox
            'X-Reference-Id: '.get_uuid(),
            'X-Target-Environment: sandbox',
            'Ocp-Apim-Subscription-Key: '.COLLECTION_SUBSCRIPTION_KEY,

        )
    );

    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);

    $curl_response = curl_exec($curl); //will respond with HTTP 202 Accepted
    // close curl resource to free up system resources

    curl_close($curl);
}


function get_uuid() {
    return sprintf( '%04x%04x-%04x-%04x-%04x-%04x%04x%04x',
        // 32 bits for "time_low"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ),

        // 16 bits for "time_mid"
        mt_rand( 0, 0xffff ),

        // 16 bits for "time_hi_and_version",
        // four most significant bits holds version number 4
        mt_rand( 0, 0x0fff ) | 0x4000,

        // 16 bits, 8 bits for "clk_seq_hi_res",
        // 8 bits for "clk_seq_low",
        // two most significant bits holds zero and one for variant DCE1.1
        mt_rand( 0, 0x3fff ) | 0x8000,

        // 48 bits for "node"
        mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff ), mt_rand( 0, 0xffff )
    );
}