<?php
// This file is part of Moodle - http://moodle.org/
//
// Moodle is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// Moodle is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with Moodle.  If not, see <http://www.gnu.org/licenses/>.

// Get the information
$currency = $instance->currency;
$amount = $cost;
$userId = $this->get_config('userid');
$apikey = $this->get_config('ApiKey');
$subscriptionkey = $this->get_config('subscriptionkey');

// echo "<pre>";
// var_dump($this);
// echo "User id : ".$userId.'<br>';
// echo "Api Key : ".$apikey.'<br>';
// echo "Subscription Key : ".$subscriptionkey.'<br>';


?>

<!-- Load the jQuery library from the Google CDN -->
<script src="//ajax.googleapis.com/ajax/libs/jquery/1.10.1/jquery.min.js"></script>

<div align="center" id="enrolment">
	<br>This course requires a payment for entry.<br>
	<b><?php echo $instancename; ?></b><br>
	<b><?php echo get_string("cost").": {$localisedcost}  {$instance->currency}"; ?></b><br>

	<img alt="mobilemoney" wwidth=90 height=90 src="<?php echo $CFG->wwwroot; ?>/enrol/mobilemoney/pix/momo1.png" />
	
	<form action="<?php echo $CFG->wwwroot . '/enrol/mobilemoney/payprocess.php'?>" method="POST">
	
		<Label for="phone">Enter your mtn number</Label><br>
		<input type="number" id="phone" name="phone" placeholder="237XXXXXXXXX"><br>

		<input type="hidden" name="apikey" value="<?php echo $apikey; ?>">
		<input type="hidden" name="userid" value="<?php echo $userId; ?>">
		<input type="hidden" name="subscriptionkey" value="<?php echo $subscriptionkey; ?>">
		<input type="hidden" name="amount" value="<?php echo $amount; ?>">
		<input type="hidden" name="currency" value="<?php echo $currency; ?>">
		<input type="hidden" name="callback" value="<?php echo $CFG->wwwroot; ?>">

		
		<div style="margin-top: 15px;">
			<input type="submit" value="make payment with mobile money"/>
		</div>
		
	</form>
	
</div>
<style>
	/* #enrolment{
		background-color: red;
		display: inline-block;
	} */
</style>
